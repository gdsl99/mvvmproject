import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mvvmproject/models/picture_model.dart';

class Services {
  Future<List<PictureModel>> fetchPicturesApi() async {
    String url = "https://picsum.photos/v2/list";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      final json = jsonDecode(response.body) as List<dynamic>;
      final listResult = json.map((e) => PictureModel.fromJson(e)).toList();
      return listResult;
    } else {
      throw Exception("Error fetching pictures");
    }
  }
}
