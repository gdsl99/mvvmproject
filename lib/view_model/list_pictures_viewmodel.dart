import 'dart:ui';

import '../models/picture_model.dart';
import '../services/services.dart';

class ListPictureViewModel {
  List<PictureViewModel>? pictures;

  Future<void> fetchPictures() async {
    final apiResult = await Services().fetchPicturesApi();
    pictures = apiResult.map((e) => PictureViewModel(e)).toList();
  }
}

class PictureViewModel {
  final PictureModel? pictureModel;
  PictureViewModel(this.pictureModel);
}
